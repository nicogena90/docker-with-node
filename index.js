const express = require("express");
const app = express();

const port = 5000;

const appRoutes = require('./routes');

// Body parser
app.use(express.urlencoded({ extended: false }));

// Añadimos las rutas del fichero routes.js
app.use('/', appRoutes);

// Listen on port 5000
app.listen(port, () => {
  console.log(`Servidor inicializado en el puerto 5000.  Accede desde la URL http://localhost:5000`);
});