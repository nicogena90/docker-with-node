# Docker with Node

Ejemplo practico de la implementacion de docker en un proyecto con node

# Que es Docker

Docker nos permite empaquetar aplicaciones con su ambiente y dependencias en una "caja", que llamaremos contenedor.
Un contenedor consiste en una aplicacion corriendo con una version basica de un Linux.
Una imagen es el "plano" de un contenedor, y este es una instancia corriendo de una imagen.

# Comandos de docker

- Crear la imagen de docker

docker build -t docker-with-node .

- Crear un contenedor desde una imagen

docker run -p 80:5000 -d docker-with-node

El primer puerto corresponde que expone el contenedor y lo conecta con el siguiente que es el interno del contenedor

- Reiniciar el servicio de docker

sudo systemctl restart docker

- Listar contenedores detenidos

docker ps -a

- Listar contenedores corriendo

docker ps

- Eliminar todos los contenedores

docker system prune

- Eliminar una imagen

docker images rm <image ID>